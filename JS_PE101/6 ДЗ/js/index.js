"use strict";

function createNewUser() {
    let str = prompt("Введіть своє і'мя");
    let str2 = prompt("Введіть свою прізвище");
    let birthday = prompt('Ваша дата народження?\nу форматі dd.mm.yyyy');

    const newUser = {
        lastName: str,
        firstName: str2,
        dateBirth: birthday,
        
        getLogin () {
            return this.lastName[0].toLowerCase()+this.firstName.toLowerCase();
        },
        getAge() {
            // В цьому методі три способи які повертатимуть скільки користувачеві років.
            // Спосіб 1

            // birthday = birthday.slice(3, 6)+birthday.slice(0, 3)+birthday.slice(6);
            // return ((new Date().getTime() - new Date(birthday)) / Math.floor(24 * 3600 * 365.25 * 1000)) | 0;

            // Спосіб 2

            birthday = birthday.slice(3, 6)+birthday.slice(0, 3)+birthday.slice(6);
            return Math.floor((new Date() - Date.parse(birthday)) / 1000 / 60 / 60 / 24 / 365.25);

            // Спосіб 3

            // let birthDate = birthday.split('.');
            // return ((new Date().getTime() - new Date(`${birthDate[2]}-${birthDate[1]}-${birthDate[0]}`)) / (24 * 3600 * 365.25 * 1000)) | 0;
        },
        getPassword() {
            return this.lastName[0].toUpperCase()+this.firstName.toLowerCase()+this.dateBirth.slice(6);
        },
    };
    return newUser; 
};

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
// console.log(newUser.getLogin());
