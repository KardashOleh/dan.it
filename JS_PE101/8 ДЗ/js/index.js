"use strict";

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000.

const p = document.querySelectorAll('p');
console.log(p);
p.forEach( item => item.style.backgroundColor = '#ff0000');

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const options = document.getElementById('optionsList');
console.log(options);

const children = options.childNodes;
console.log(children);

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph.

const testParagraph = document.getElementById('testParagraph');
console.log(testParagraph);
testParagraph.textContent = 'This is a paragraph.';

// 4. Отримати елементи.
// 5. Вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const header = document.querySelectorAll('.main-header');
console.log(header);
header.forEach( item => item.classList.add('nav-item'));
console.log(header);

// 6. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const title = document.querySelectorAll('.section-title');
console.log(title);
title.forEach( item => item.classList.remove('section-title'));
console.log(title);
