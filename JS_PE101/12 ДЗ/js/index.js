"use strict";

const buttons = document.querySelectorAll('.btn');

document.addEventListener("keydown", event => {
    let keyCode = event.key;
    console.log(keyCode);
    for (let btn of buttons) {
        if(btn.textContent.toLowerCase() === keyCode.toLowerCase()){
            for (const btn of buttons) {
                btn.classList.remove("dark");
            };
            btn.classList.add("dark");
        };
    };
});
