"use strict";

const btnStop = document.querySelector('.stop');
const btnStart = document.querySelector('.start');
const imageCollection = document.querySelectorAll('.image-to-show');
let startStop = '';

// =============== 1 спосіб ========================

// let counterImg = 0;
// function showImg() {
//     for (const img of imageCollection) {
//         img.classList.add('hidden');
//     };
//     imageCollection[counterImg].classList.remove('hidden');
//     counterImg++;
//     if (counterImg >= imageCollection.length) {
//         counterImg = 0;
//     };
// };
// showImg();

// =============== 2 спосіб ========================

let counterImg = 1;
function showImg() {
    for (const img of imageCollection) {
        img.classList.remove('active');
    };
    imageCollection[counterImg].classList.add('active');
    counterImg++;
    if (counterImg >= imageCollection.length) {
        counterImg = 0;
    };
};

document.addEventListener('DOMContentLoaded', () => {
    startStop = setInterval(showImg, 3000);
});
btnStart.addEventListener('click', () => {
    clearInterval(startStop)
    startStop = setInterval(showImg, 3000);
});
btnStop.addEventListener('click', () => {
    clearInterval(startStop);
}); 
