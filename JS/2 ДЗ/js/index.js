"use strict";

let a 

do { 
    a = +prompt("Введите число"); 
} while (!Number.isInteger(a));
 
if (a < 5) { 
    console.log("Sorry, no numbers"); 
} else { 
    for (let i = 0; i <= a; i++) { 
        if (i % 5 !== 0) 
        continue; 
        console.log(i); 
    } 
}
